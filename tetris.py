"""
BaseCase Programming Test.
"""
import random


pieces = [
    [
        '****',
    ],
    [
        '* ',
        '* ',
        '* ',
        '**',
    ],
    [
        ' *',
        ' *',
        ' *',
        '**',
    ],
    [
        ' *',
        '**',
        '* ',
    ],
    [
        '**',
        '**'
    ],
]

def get_random_piece():
    return pieces[random.randint(0,4)]

def create_board():
    board = []
    for e in range(20):
        normal_line = ''.join(['*']+[' ' for i in range(20)]+['*'])
        board.append(normal_line)
    bottom_line = ''.join(['*' for i in range(22)])
    board.append(bottom_line)

    return board

def draw_board(board):
    for line in board:
        print line

def put_piece_in_board(board, piece, line, pos):
    line_pos = line
    for piece_line in piece:
        aux_pos = pos
        for char in piece_line:
            board_line = list(board[line_pos])
            if board_line[aux_pos] == char == '*':
                raise Exception('You raise the bottom line!')
            board_line[aux_pos] = char
            board[line_pos] = ''.join(board_line)
            aux_pos += 1
        line_pos += 1
    return board

def rotate_clockwise(piece, counter=False):
    new_piece = [ '' for i in piece[0] ]
    index = 0 if counter else len(piece[0])-1
    for c in piece[0]:
        line_index = 0
        for piece_line in piece:
            if counter:
                new_piece[index] += piece_line[-1]
            else:
                new_piece[index] = piece_line[-1] + new_piece[index]
            piece[line_index] = piece_line[:-1]
            line_index += 1
        index += 1 if counter else -1

    return new_piece


if __name__ == "__main__":
    board = create_board()
    # Put the random piece on the first line.
    piece = get_random_piece()
    line = 0
    pos = random.randint(1, 17)
    put_piece_in_board(board, piece, line, pos)
    draw_board(board)

    while True:
        value = raw_input('Enter your move (A, D, W or S): ').upper()
        if value == 'D':
            if not (pos + len(piece[0]) > 20):
                pos += 1
        elif value == 'A':
            if not (pos == 1):
                pos -= 1
        elif value == 'W':
            piece = rotate_clockwise(piece, counter=True)
        elif value == 'S':
            piece = rotate_clockwise(piece)

        line += 1
        piece_board = create_board()
        put_piece_in_board(piece_board, piece, line, pos)
        draw_board(piece_board)
